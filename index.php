<?php include 'includes/header.php' ?>

<?php include 'includes/slide.php' ?>

<section class="container book-hotel">
    <div class="row">
        <div class="col-xs-12">
            <div class="wrap-bookHotel">
                <a href="#" class="btn-bookHotel">Đặt phòng cho tôi</a>
            </div>
        </div>
    </div>
</section>

<section class="container wrap-introduce">
    <div class="introduce">
        <div class="box-title">
            <h1>Tổng quan về khách sạn Thắng Lợi</h1>
            <div class="line-shape"><img src="images/line-shape.png" alt=""></div>
        </div>
        <p>
            Khách sạn Thắng Lợi Hà Nội nằm ven hồ Tây, hồ rộng và lãng mạn nhất Hà Nội. Từ khách sạn, quý khách chỉ đi mất 30 phút để đên sân bay quốc tế Nội Bài và 10 phút để đi vào trung tâm thành phố. Là khách sạn tiêu chuẩn quốc tế bốn sao, với 175 phòng nghỉ. Từ khách sạn Quý khách có thể bao quát được toàn cảnh hồ Tây hay thư giãn trong không gian tươi tốt của khu vườn. Khách sạn Thắng Lợi là địa điểm lý tưởng để quý khách lưu lại trong những chuyến công tác dài ngày hoặc để cảm nhận 1 cảm giác khác biệt ngay trong thành phố.
        </p>
    </div>
    <div id="grid-gal">
        <div class="gal-item height-item-1">
            <img src="images/gal/gal-1.png" alt="">
        </div>
        <div class="gal-item height-item-2">
            <img src="images/gal/gal-2.png" alt="">
        </div>
        <div class="gal-item height-item-2">
            <img src="images/gal/gal-3.png" alt="">
        </div>
        <div class="gal-item height-item-2">
            <img src="images/gal/gal-4.png" alt="">
        </div>
        <div class="gal-item height-item-2">
            <img src="images/gal/gal-5.png" alt="">
        </div>
    </div>
</section>

<section class="service">
    <div class="container wrap-inner-service">
        <div class="inner">
            <div class="box-title">
                <h1>dịch vụ - tiện nghi</h1>
                <div class="line-shape"><img src="images/line-shape.png" alt=""></div>
            </div>

            <!-- TAB NAVIGATION -->
            <ul class="nav nav-tabs nav-justified tab-service" role="tablist">
                <li class="active">
                    <a href="#tab1" role="tab" data-toggle="tab">
                        <span class="tab-img t-1"></span>
                        Họp, hội thảo
                    </a>
                </li>
                <li>
                    <a href="#tab2" role="tab" data-toggle="tab">
                        <span class="tab-img t-2"></span>
                        Bufer, tiệc
                    </a>
                </li>
                <li>
                    <a href="#tab3" role="tab" data-toggle="tab">
                        <span class="tab-img t-3"></span>
                        Bar
                    </a>
                </li>
                <li>
                    <a href="#tab4" role="tab" data-toggle="tab">
                        <span class="tab-img t-4"></span>
                        Golf
                    </a>
                </li>
                <li>
                    <a href="#tab5" role="tab" data-toggle="tab">
                        <span class="tab-img t-5"></span>
                        Sauna Massage
                    </a>
                </li>
                <li>
                    <a href="#tab6" role="tab" data-toggle="tab">
                        <span class="tab-img t-6"></span>
                        Tenis
                    </a>
                </li>
                <li>
                    <a href="#tab7" role="tab" data-toggle="tab">
                        <span class="tab-img t-7"></span>
                        Bể bơi
                    </a>
                </li>
            </ul>
            <!-- TAB CONTENT -->
            <div class="tab-content content-ser-pad">
                <div class="active tab-pane fade in" id="tab1">
                    <div class="row content-ser-tab">
                        <div class="col-xs-5">
                            <img class="img-responsive" src="images/img-tab-ser.png" alt="">
                        </div>
                        <div class="col-xs-7">
                            <div class="info">
                                <h2>Khách sạn Thắng Lợi</h2>
                                <h4>Phòng họp với sức chứa 100 người</h4>
                                <p>
                                    Khách sạn Thắng Lợi với nhiều phòng có sức chứa khác nhau, đáp ứng từng yêu cầu của quý khách hàng. Phòng họp được trang bị các loại màn hình, máy chiếu sắc nét và có độ phân giải lớn, Internet tốc độ cao, Wifi và hệ thống đàm thoại hội nghị.
                                </p>
                                <p>
                                    Luôn sẵn sàng bố trí các loại phòng họp tiện nghi sang trọng và hiện đại cho khách hàng lựa chọn
                                </p>
                                <a href="#" class="btn btn-default btn-more">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab2">
                    <h2>Tab2</h2>

                    <p>Lorem ipsum.</p>
                </div>
                <div class="tab-pane fade" id="tab3">
                    <h2>Tab3</h2>

                    <p>Lorem ipsum.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="new-event">
    <div class="container">
        <div class="box-title">
            <h1>TIN TỨC - SỰ KIỆN</h1>
            <div class="line-shape"><img src="images/line-shape.png" alt=""></div>
        </div>

        <div id="list-new" class="owl-carousel">
            <div class="new-item">
                <div class="inner-new-item">
                    <div class="img">
                        <a href="#">
                            <img src="images/gal/gal-4.png" alt="">
                        </a>
                    </div>
                    <div class="info">
                        <h4><a href="#" class="line-clamp line-clamp-2">Giảm giá dịchSauna Massage nhân dịp 2/9</a></h4>
                        <p class="line-clamp line-clamp-3">
                            Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                        </p>
                    </div>
                </div>
            </div>
            <div class="new-item">
                <div class="inner-new-item">
                    <div class="img">
                        <a href="#">
                            <img src="images/gal/gal-4.png" alt="">
                        </a>
                    </div>
                    <div class="info">
                        <h4><a href="#" class="line-clamp line-clamp-2">Giảm giá dịchSauna Massage nhân dịp 2/9</a></h4>
                        <p class="line-clamp line-clamp-3">
                            Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                        </p>
                    </div>
                </div>
            </div>
            <div class="new-item">
                <div class="inner-new-item">
                    <div class="img">
                        <a href="#">
                            <img src="images/gal/gal-4.png" alt="">
                        </a>
                    </div>
                    <div class="info">
                        <h4><a href="#" class="line-clamp line-clamp-2">Giảm giá dịchSauna Massage nhân dịp 2/9</a></h4>
                        <p class="line-clamp line-clamp-3">
                            Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                        </p>
                    </div>
                </div>
            </div>
            <div class="new-item">
                <div class="inner-new-item">
                    <div class="img">
                        <a href="#">
                            <img src="images/gal/gal-4.png" alt="">
                        </a>
                    </div>
                    <div class="info">
                        <h4><a href="#" class="line-clamp line-clamp-2">Giảm giá dịchSauna Massage nhân dịp 2/9</a></h4>
                        <p class="line-clamp line-clamp-3">
                            Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                        </p>
                    </div>
                </div>
            </div>
            <div class="new-item">
                <div class="inner-new-item">
                    <div class="img">
                        <a href="#">
                            <img src="images/gal/gal-4.png" alt="">
                        </a>
                    </div>
                    <div class="info">
                        <h4><a href="#" class="line-clamp line-clamp-2">Giảm giá dịchSauna Massage nhân dịp 2/9</a></h4>
                        <p class="line-clamp line-clamp-3">
                            Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'includes/footer.php' ?>