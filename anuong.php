<?php include 'includes/header.php' ?>

<?php include 'includes/slide.php' ?>

<section class="container book-hotel">
    <div class="row">
        <div class="col-xs-12">
            <div class="wrap-bookHotel">
                <a href="#" class="btn-bookHotel">Đặt phòng cho tôi</a>
            </div>
        </div>
    </div>
</section>

<section class="block-breadcrumb">
    <div class="container">
        <div class="rcrumbs" id="breadcrumbs">
            <ul>
                <li><a href="#">Home</a><span class="divider">/</span></li>
                <li><a href="#">Ăn uống</a></li>
            </ul>
        </div>
    </div>
</section>

<section class="container">
    <div class="row">
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="food-item">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/anuong/anuong-1.png" alt=""></a>
                    </div>
                    <div class="col-xs-6">
                        <div class="info">
                            <h1><a href="#">Món súp thơm ngon</a></h1>
                            <p class="line-clamp line-clamp-3">
                                Quý khách cũng có thể thưởng thức sự thư giãn tuyệt vời tại Sauna, Massage & Spa trị liệu ở khách sạn với giá ưu đãi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'includes/footer.php' ?>
