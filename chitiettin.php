<?php include 'includes/header.php' ?>

<?php include 'includes/slide.php' ?>

<section class="container book-hotel">
   <div class="row">
      <div class="col-xs-12">
         <div class="wrap-bookHotel">
            <a href="#" class="btn-bookHotel">Đặt phòng cho tôi</a>
         </div>
      </div>
   </div>
</section>

<section class="block-breadcrumb">
   <div class="container">
      <div class="rcrumbs" id="breadcrumbs">
         <ul>
            <li><a href="#">Home</a><span class="divider">/</span></li>
            <li><a href="#">Tin tức - Sự kiện</a></li>
         </ul>
      </div>
   </div>
</section>

<section class="container">
   <div class="row">
      <div class="col-xs-3">
         <div class="box-contact">
            <h4>Liên Hệ</h4>
            <ul class="nav">
               <li>Địa chỉ: Phố Yên Phụ, Quận Tây Hồ, Hà Nội</li>
               <li>Tel: 04 38294211 - 38290145</li>
               <li>Fax: 04 38292927</li>
               <li>E: info@thangloihotel.vn</li>
            </ul>
         </div>

         <div class="box-bookRoom">
            <div class="text-center">
               <h4>Đặt phòng nhanh</h4>
               <div class="line"></div>
               <p>
                  Hãy trải nghiệm không gian tuyệt vời và dịch vụ cao cấp tại Chuỗi khách sạn Thắng Lợi nằm ngay giữa trung tâm Thành phố
               </p>
               <img src="images/4star.png" alt="">
            </div>
            <div class="form-book">
               <form action="">
                  <div class="row">
                     <div class="col-xs-12 mb-10">
                        <div class="input-icon icon-right">
                           <input type="text" name="" id="" class="form-control non-pl bookHotelDatePicker" placeholder="Ngày đến" />
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                     <div class="col-xs-12 mb-10">
                        <div class="input-icon icon-right">
                           <input type="text" name="" id="" class="form-control non-pl bookHotelDatePicker" placeholder="Ngày đi" />
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                  </div>
                  <div class="row non-padding-col mb-10">
                     <div class="col-xs-6 non-padding-right">
                        <select class="form-control">
                           <option value="">Số phòng</option>
                           <option value="">101</option>
                           <option value="">201</option>
                           <option value="">301</option>
                        </select>
                     </div>
                     <div class="col-xs-6 non-padding-left">
                        <select class="form-control">
                           <option value="">Số người</option>
                           <option value="">1</option>
                           <option value="">2</option>
                           <option value="">3</option>
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12">
                        <button type="submit" class="btn btn-default btn-block btn-submit">đặt phòng cho tôi</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="col-xs-9">
         <div class="new-contentDetail">
            <h3 class="mt-0">Khách sạn Thắng Lợi đổi tên</h3>
            <p style="color: #878787;font-size: 14px;">Thứ ba, 15/12/2015</p>
            <p>
               <b>Khách sạn Thắng Lợi sẽ chính thức hoạt động với thương hiệu mới là Hilton Hanoi Westlake.</b>
            </p>
            <p>
               Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilton.
            </p>
            <div class="text-center" style="margin-bottom: 20px;">
               <img src="images/newdetail/imgdetail-1.jpg" alt="">
            </div>
            <p>
               Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi. Dự kiến khách sạn Hilton Hanoi Westlake sẽ có 250 phòng và mở cửa đón khách vào năm 2018.
            </p>
            <p>
               Bà Nguyễn Thị Nga, Chủ tịch Hội đồng quản trị Công ty Khách sạn Thắng Lợi, cho biết: "Khu vực Hồ Tây là một vị trí được coi là ưu việt bậc nhất cho một tổ hợp khách sạn đẳng cấp. Với sự quản lý của Tập đoàn Hilton, khi hoàn thành khách sạn sẽ góp phần thay đổi cảnh quan, là một quần thể các tiện nghi và dịch vụ cao cấp, thu hút du lịch, các hội nghị quan trọng trong và ngoài nước".
            </p>
            <p>
               Ông William Costley, Phó chủ tịch Vận hành của Tập đoàn Hilton Worldwide nhận định: "Khách sạn Thắng Lợi là vị trí lý tưởng cho mô hình khách sạn nghỉ dưỡng trong thành phố". Kể từ khi chuyển đổi sang mô hình công ty cổ phần, Công ty cổ phần Khách sạn Thắng Lợi đã tích cực làm việc với các Tập đoàn hàng đầu thế giới trong lĩnh vực khách sạn, golf, khu vui chơi giải trí.
            </p>
            <p>
               Khách sạn Thắng Lợi có lịch sử gần 40 năm, đạt tiêu chuẩn 4 sao, nằm trên đường Yên Phụ (Q. Tây Hồ, Hà Nội). Đây được đánh giá là một trong những địa điểm nghỉ dưỡng có vị trí đắc địa của thành phố, ven Hồ Tây với tổng diện tích khoảng 4,5ha.
            </p>
            <p>
               Từ khi đi vào hoạt động, khách sạn nhiều lần được thay đổi mô hình quản lý. Trong giai đoạn hạch toán tập trung bao cấp, từ năm 1975 đến 1986, khách sạn trực thuộc Chính phủ; từ 1986 đến 1995 được chuyển giao cho Công ty du lịch Hà Nội quản lý và hoạt động theo cơ chế của thị trường. Từ năm 1995 đến nay, khách sạn được hoạch toán độc lập, tách ra khỏi Công ty du lịch Hà Nội để thành lập doanh nghiệp riêng theo mô hình quản lý mới.
            </p>
         </div>

         <div class="new-other">
            <div class="header">
               <h4>các tin khác</h4>
               <div class="line"></div>
            </div>
            <div class="content">
               <ul>
                  <li><a href="#">Khách sạn Thắng Lợi Hà Nội Quận Tây Hồ: Đặt trực tuyến giá tốt nhất</a></li>
                  <li><a href="#">Khách sạn Thắng Lợi Hà Nội Quận Tây Hồ: Đặt trực tuyến giá tốt nhất</a></li>
                  <li><a href="#">Khách sạn Thắng Lợi Hà Nội Quận Tây Hồ: Đặt trực tuyến giá tốt nhất</a></li>
                  <li><a href="#">Khách sạn Thắng Lợi Hà Nội Quận Tây Hồ: Đặt trực tuyến giá tốt nhất</a></li>
                  <li><a href="#">Khách sạn Thắng Lợi Hà Nội Quận Tây Hồ: Đặt trực tuyến giá tốt nhất</a></li>
                  <li><a href="#">Khách sạn Thắng Lợi Hà Nội Quận Tây Hồ: Đặt trực tuyến giá tốt nhất</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>

<?php include 'includes/footer.php' ?>
