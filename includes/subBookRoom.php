<section class="sub-bookHotel">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="address-hotel">
                    <h1>Trải nghiệm những ngày tuyệt vời tại khách sạn chúng tôi</h1>
                    <p>
                        Địa chỉ: Phố Yên Phụ, Quận Tây Hồ, Hà Nội
                    </p>
                    <p>
                        Tel: 04 38294211 - 38290145; Fax: 04 38292927  | E: info@thangloihotel.vn
                    </p>
                </div>

                <div class="form-subBookHotel">
                    <form action="">
                        <div class="row row-small">
                            <div class="col-xs-6">
                                <div class="row row-small">
                                    <div class="col-xs-6">
                                        <div class="input-icon icon-right">
                                            <span>Ngày đến </span>
                                            <input type="text" name="" id="" class="form-control bookHotelDatePicker" />
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="input-icon icon-right">
                                            <span>Ngày đến </span>
                                            <input type="text" name="" id="" class="form-control bookHotelDatePicker" />
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="row row-small">
                                    <div class="col-xs-4">
                                        <select name="" id="" class="form-control">
                                            <option value="">Số phòng</option>
                                            <option value="">101</option>
                                            <option value="">201</option>
                                            <option value="">301</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <select name="" id="" class="form-control">
                                            <option value="">Trẻ em</option>
                                            <option value="">101</option>
                                            <option value="">201</option>
                                            <option value="">301</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-default btn-block btn-suBookRoom"><i class="fa fa-check-square-o"></i> Đặt phòng cho tôi</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>