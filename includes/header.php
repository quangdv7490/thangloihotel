<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home</title>
    <!-- Font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Level Plugins -->
    <link href="plugins/layerslider/css/layerslider.css" rel="stylesheet">
    <link href="plugins/owlcarousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="plugins/rcrumbs/jquery.rcrumbs.css" rel="stylesheet">
    <link href="plugins/air-datepicker/css/datepicker.min.css" rel="stylesheet">
    <link href="plugins/flexslider/flexslider.css" rel="stylesheet">
    <!-- Custom Style -->
    <link href="css/custom/custom-style.css" rel="stylesheet">
    <link href="css/custom/custom-other.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header id="header">
    <section class="top">
        <div class="container">
            <p class="pull-left com-flex com-flex-middle rate-hotel"><img src="images/4star.png" alt=""><b>Thắng Lợi Hotel -</b> Khách sạn đạt tiêu chuẩn 4 sao tại Hà Nội</p>
            <ul class="nav navbar-nav navbar-right com-top-right">
                <li>
                    <form action="" class="com-flex com-flex-middle">
                        <input type="text" name="" id="" placeholder="Tìm kiếm">
                        <input type="submit" value="">
                    </form>
                </li>
                <li>
                    <a href="#">VN</a>
                </li>
                <li>
                    <a href="#">EN</a>
                </li>
            </ul>
        </div>
    </section>
    <section class="navigation">
        <div class="container">
            <nav class="navbar top-navbar">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img src="images/logo.png" alt="">
                    </a>
                </div>
                <div class="collapse navbar-collapse navigation-r">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">tổng quan</a>
                        </li>
                        <li class="dropdown">
                            <a href="#">phòng ở</a>
                        </li>
                        <li class="dropdown">
                            <a href="#">ăn uống</a>
                        </li>
                        <li class="dropdown">
                            <a href="#">hội thảo & tiệc</a>
                        </li>
                        <li class="dropdown">
                            <a href="#">cơ sở vật chất</a>
                        </li>
                        <li class="dropdown">
                            <a href="#">tin tức - sự kiện</a>
                        </li>
                        <li class="dropdown">
                            <a href="#">liên hệ</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </section>
</header>