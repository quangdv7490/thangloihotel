<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <h4 class="title-footer">Kết nối với chúng tôi</h4>

                <form action="" class="form-inline footer-form-mail">
                    <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Đăng ký nhận email">
                    </div>
                    <button type="submit" class="btn btn-default btn-sendmail">Gửi Đi</button>
                </form>
            </div>

            <div class="col-xs-6 text-right">
                <h4 class="title-footer">Kết nối với chúng tôi</h4>
                <ul class="nav nav-pills social-block">
                    <li>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-facebook-f"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="line-dot"></div>

    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <img src="images/logo-footer.png" alt="">
            </div>
            <div class="col-xs-9">
                <ul class="nav nav-pills nav-footer">
                    <li><a href="#">TỔNG QUAN</a></li>
                    <li><a href="#">PHÒNG Ở</a></li>
                    <li><a href="#">ĂN UỐNG</a></li>
                    <li><a href="#">HỘI THẢO & TIỆC</a></li>
                    <li><a href="#">CƠ SỞ VẬT CHẤT</a></li>
                    <li><a href="#">TIN TỨC - Sự KIỆN</a></li>
                    <li><a href="#">LIÊN HỆ</a></li>
                </ul>
                <p class="footer-address">Địa chỉ: Phố Yên Phụ, Quận Tây Hồ, Hà Nội | Tel: 04 38294211 - 38290145; Fax: 04 38292927  | E: info@thangloihotel.vn</p>
                <p class="footer-address">© 2016 Thang Loi hotel. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!--<script src="js/jquery-migrate-1.4.1.min.js"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!-- Level Plugins -->
<script src="plugins/layerslider/js/greensock.js"></script>
<script src="plugins/layerslider/js/layerslider.transitions.js"></script>
<script src="plugins/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script src="plugins/owlcarousel/owl.carousel.min.js"></script>
<script src="plugins/mansonry/masonry.pkgd.min.js"></script>
<script src="plugins/mansonry/imagesloaded.pkgd.min.js"></script>
<script src="plugins/rcrumbs/jquery.rcrumbs.min.js"></script>
<script src="plugins/air-datepicker/js/datepicker.min.js"></script>
<script src="plugins/air-datepicker/js/i18n/datepicker.en.js"></script>
<script src="plugins/flexslider/jquery.flexslider-min.js"></script>
<!-- Main Script -->
<script>
    $(document).ready(function(){
        $("#full-width-slider").layerSlider({
            skinsPath: 'plugins/layerslider/skins/',
            responsiveUnder: 1140,
            layersContainer: 1140,
            showCircleTimer: false,
            hoverPrevNext: false,
            navButtons: false,
            navStartStop: false,
            pauseOnHover: false
        });

        var $grid = $('#grid-gal').imagesLoaded( function() {
            // init Masonry after all images have loaded
            $grid.masonry({
                itemSelector: '.gal-item'
            });
        });

        $('#list-new').owlCarousel({
            items: 4,
            loop: true,
            margin: 30,
            slideBy: 1,
            lazyLoad: true,
            lazyContent: true,
            autoplay: true,
            autoplayHoverPause: true
        });

        $("#breadcrumbs").rcrumbs();

        $(".bookHotelDatePicker").datepicker({
            language: 'en',
            autoClose: true
        });

        $('#carousel-detail').flexslider({
            animation: "slide",
            controlNav: false,
            nextText: '<img src="images/next-control.png" alt="">',
            prevText: '<img src="images/prev-control.png" alt="">',
            animationLoop: false,
            slideshow: false,
            itemWidth: 100,
            itemMargin: 5,
            asNavFor: '#slider-detail'
        });

        $('#slider-detail').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel-detail"
        });
    });
</script>
</body>
</html>