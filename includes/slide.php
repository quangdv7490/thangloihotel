<section id="banner-slide">
    <div id="full-width-slider" style="width:100%;height:500px;">
        <div class="ls-slide" data-ls="slidedelay: 4000;transition2d: 5;">
            <img src="images/slides/slide-2.jpg" class="ls-bg" alt="Slide background">
            <a href="#" class="ls-link"></a>
            <img src="images/slides/slide-2.jpg" class="ls-tn" alt="Slide thumbnail">
        </div>
        <div class="ls-slide" data-ls="slidedelay: 4000;transition2d: 5;">
            <img src="images/slides/slide-1.jpg" class="ls-bg" alt="Slide background">
            <a href="#" class="ls-link"></a>
            <img src="images/slides/slide-1.jpg" class="ls-tn" alt="Slide thumbnail">
        </div>
    </div>
</section>