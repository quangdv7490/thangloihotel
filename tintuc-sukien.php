<?php include 'includes/header.php' ?>

<?php include 'includes/slide.php' ?>

<section class="container book-hotel">
    <div class="row">
        <div class="col-xs-12">
            <div class="wrap-bookHotel">
                <a href="#" class="btn-bookHotel">Đặt phòng cho tôi</a>
            </div>
        </div>
    </div>
</section>

<section class="block-breadcrumb">
    <div class="container new-container">
        <div class="rcrumbs" id="breadcrumbs">
            <ul>
                <li><a href="#">Home</a><span class="divider">/</span></li>
                <li><a href="#">Tin tức - Sự kiện</a></li>
            </ul>
        </div>
    </div>
</section>

<section class="event-item isLeft">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isRight">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6 col-xs-push-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6 col-xs-pull-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isLeft">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isRight">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6 col-xs-push-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6 col-xs-pull-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isLeft">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isRight">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6 col-xs-push-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6 col-xs-pull-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isLeft">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isRight">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6 col-xs-push-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6 col-xs-pull-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="event-item isLeft">
    <div class="container new-container">
        <div class="row com-flex com-flex-middle">
            <div class="col-xs-6">
                <a href="#">
                    <img class="img-responsive" src="images/event/event-1.png" alt="">
                </a>
            </div>
            <div class="col-xs-6">
                <div class="info">
                    <h1><a href="#">Khách sạn Thắng Lợi đổi tên</a></h1>
                    <div class="line-event"></div>
                    <p class="post-time">Đăng ngày 20 - 05 - 2016 bởi Admin</p>
                    <p class="line-clamp line-clamp-5">
                        Ngày 15/12, Công ty  Khách sạn Thắng Lợi và Tập đoàn Hilton Worldwide đã ký kết thỏa thuận quản lý khách sạn Hilton theo tiêu chuẩn 5 sao. Như vậy, Khách sạn Thắng Lợi đã chính thức chuyển mình với thương hiệu Hilton Hanoi Westlake - thuộc Tập đoàn Hilto.  Đây là khách sạn thứ 3 mang thương hiệu của Hilton tại Hà Nội, sau khách sạn Hilton Hanoi Opera và Hilton Garden Inn Hanoi.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container new-container">
    <div class="row">
        <div class="col-xs-12 text-right">
            <ul class="pagination common-pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>

<?php include 'includes/footer.php' ?>