<?php include 'includes/header.php' ?>

<?php include 'includes/slide.php' ?>

<section class="container book-hotel">
   <div class="row">
      <div class="col-xs-12">
         <div class="wrap-bookHotel">
            <a href="#" class="btn-bookHotel">Đặt phòng cho tôi</a>
         </div>
      </div>
   </div>
</section>

<section class="block-breadcrumb">
   <div class="container">
      <div class="rcrumbs" id="breadcrumbs">
         <ul>
            <li><a href="#">Home</a><span class="divider">/</span></li>
            <li><a href="#">Phòng ở</a><span class="divider">/</span></li>
            <li><a href="#">Phòng Duluxy</a></li>
         </ul>
      </div>
   </div>
</section>

<section class="container">
   <div class="row">
      <div class="col-xs-3">
         <div class="box-contact">
            <h4>Phòng ở</h4>
            <ul class="common-ul">
               <li>Phòng Deluxe</li>
               <li>Phòng Executive</li>
               <li>Căn hộ Studio</li>
               <li>Căn hộ Executive</li>
               <li>Phòng Suite</li>
               <li>Royal Suite</li>
            </ul>
         </div>

         <div class="box-bookRoom">
            <div class="text-center">
               <h4>Đặt phòng nhanh</h4>
               <div class="line"></div>
               <p>
                  Hãy trải nghiệm không gian tuyệt vời và dịch vụ cao cấp tại Chuỗi khách sạn Thắng Lợi nằm ngay giữa trung tâm Thành phố
               </p>
               <img src="images/4star.png" alt="">
            </div>
            <div class="form-book">
               <form action="">
                  <div class="row">
                     <div class="col-xs-12 mb-10">
                        <div class="input-icon icon-right">
                           <input type="text" name="" id="" class="form-control non-pl bookHotelDatePicker" placeholder="Ngày đến" />
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                     <div class="col-xs-12 mb-10">
                        <div class="input-icon icon-right">
                           <input type="text" name="" id="" class="form-control non-pl bookHotelDatePicker" placeholder="Ngày đi" />
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                  </div>
                  <div class="row non-padding-col mb-10">
                     <div class="col-xs-6 non-padding-right">
                        <select class="form-control">
                           <option value="">Số phòng</option>
                           <option value="">101</option>
                           <option value="">201</option>
                           <option value="">301</option>
                        </select>
                     </div>
                     <div class="col-xs-6 non-padding-left">
                        <select class="form-control">
                           <option value="">Số người</option>
                           <option value="">1</option>
                           <option value="">2</option>
                           <option value="">3</option>
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12">
                        <button type="submit" class="btn btn-default btn-block btn-submit">đặt phòng cho tôi</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="col-xs-9">
         <div id="slider-detail" class="flexslider">
            <ul class="slides">
               <li>
                  <img src="images/room/roomdetail-1.jpg" />
               </li>
               <li>
                  <img src="images/room/roomdetail-1.jpg" />
               </li>
               <li>
                  <img src="images/room/roomdetail-1.jpg" />
               </li>
               <li>
                  <img src="images/room/roomdetail-1.jpg" />
               </li>
            </ul>
         </div>
         <div class="wrap-thumb-detail">
            <div class="inner-thumb-detail">
               <div id="carousel-detail" class="flexslider">
                  <ul class="slides">
                     <li>
                        <img src="images/room/roomdetail-1.jpg" />
                     </li>
                     <li>
                        <img src="images/room/roomdetail-1.jpg" />
                     </li>
                     <li>
                        <img src="images/room/roomdetail-1.jpg" />
                     </li>
                     <li>
                        <img src="images/room/roomdetail-1.jpg" />
                     </li>
                  </ul>
               </div>
            </div>
         </div>

         <div class="row mb-30">
            <div class="col-xs-12">
               <div class="form-subBookHotel">
                  <form action="">
                     <div class="row row-small">
                        <div class="col-xs-6">
                           <div class="row row-small">
                              <div class="col-xs-6">
                                 <div class="input-icon icon-right">
                                    <span>Ngày đến </span>
                                    <input type="text" name="" id="" class="form-control bookHotelDatePicker" />
                                    <i class="fa fa-calendar"></i>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="input-icon icon-right">
                                    <span>Ngày đến </span>
                                    <input type="text" name="" id="" class="form-control bookHotelDatePicker" />
                                    <i class="fa fa-calendar"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="row row-small">
                              <div class="col-xs-4">
                                 <select name="" id="" class="form-control">
                                    <option value="">Số phòng</option>
                                    <option value="">101</option>
                                    <option value="">201</option>
                                    <option value="">301</option>
                                 </select>
                              </div>
                              <div class="col-xs-4">
                                 <select name="" id="" class="form-control">
                                    <option value="">Trẻ em</option>
                                    <option value="">101</option>
                                    <option value="">201</option>
                                    <option value="">301</option>
                                 </select>
                              </div>
                              <div class="col-xs-4">
                                 <button type="submit" class="btn btn-default btn-block btn-suBookRoom"><i class="fa fa-check-square-o"></i> Đặt phòng</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-xs-8">
               <div class="box mb-20">
                  <h4 class="box-title-common">Giới thiệu</h4>
                  <p>
                     Nằm ở tầng 4 đến tầng 12, Phòng Deluxe Mặt Sông, được thiết kế theo phong cách hiện đại mới trang nhã, có cửa sổ kính bố trí suốt từ sàn lên trần đem đến quang cảnh thành phố tuyệt đẹp hay quang cảnh sông thơ mộng.
                     Mỗi phòng đều có khu làm việc và khu thư giãn riêng biệt với ti vi truyền hình cáp/vệ tinh màn hình LCD, và một giường cỡ lớn hoặc hai giường đôi được trang bị gối sợi cực mịn và chăn cao cấp. Phòng tắm bằng đá cẩm thạch rộng rãi có một bồn tắm, bàn trang điểm lớn và bồn vệ sinh bidet điện có điện thoại. Du khách có thể lựa chọn tầng hút thuốc hoặc không hút thuốc.
                  </p>
               </div>
               <div class="box mb-20">
                  <h4 class="box-title-common">Dịch vụ đặc biệt</h4>
                  <ul class="nav list-special-service">
                     <li>Dịch vụ xe buýt khách sạn-thành phố miễn phí</li>
                     <li>Trung tâm thể dục và bể bơi ngoài trời có sẵn miễn phí</li>
                     <li>Internet không dây/có dây miễn phí trong tất cả các phòng</li>
                     <li>Dịch Vụ Chỉnh Trang Phòng Buổi Tối được cung cấp</li>
                  </ul>
               </div>
            </div>

            <div class="col-xs-4">
               <div class="box-info">
                  <ul class="nav nav-info">
                     <li class="clearfix">
                        <span class="pull-left">Loại giường</span>
                        <span class="pull-right">Giường đôi</span>
                     </li>
                     <li class="clearfix">
                        <span class="pull-left">View</span>
                        <span class="pull-right">Hồ tây / Đường phố</span>
                     </li>
                     <li class="clearfix">
                        <span class="pull-left">Số chỗ tối đa</span>
                        <span class="pull-right">3 người</span>
                     </li>
                     <li class="clearfix">
                        <span class="pull-left">Kích thước</span>
                        <span class="pull-right">37 m2</span>
                     </li>
                  </ul>
               </div>

               <div class="box-info">
                  <h4 class="title">CHECK-IN / CHECK-OUT</h4>
                  <ul class="nav nav-info">
                     <li>Nhận phòng: sau 14h</li>
                     <li>Trả  phòng: trước 12h</li>
                  </ul>
               </div>

               <div class="box-info">
                  <h4 class="title">HỖ TRỢ TRỰC TUYẾN</h4>
                  <ul class="nav nav-info">
                     <li>Tel: 043 433 9382</li>
                     <li>Hotline: 0943 933 9292</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<?php include 'includes/footer.php' ?>
