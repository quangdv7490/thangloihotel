<?php include 'includes/header.php' ?>

<?php include 'includes/subBookRoom.php' ?>

<section class="block-breadcrumb">
    <div class="container">
        <div class="rcrumbs" id="breadcrumbs">
            <ul>
                <li><a href="#">Home</a><span class="divider">/</span></li>
                <li><a href="#">Ăn uống</a></li>
            </ul>
        </div>
    </div>
</section>

<section class="container wrap-room-hotel">
    <div class="row">
        <div class="col-xs-4">
            <div class="room-item">
                <div class="img">
                    <a href="#"><img src="images/room/room-1.png" alt=""></a>
                </div>
                <div class="info">
                    <h1><a href="#">Phòng Deluxe</a></h1>
                    <ul>
                        <li>Một phòng ngủ, một phòng tắm</li>
                        <li>View quang cảnh vườn hoặc thành phố</li>
                        <li>Giường ngủ rộng</li>
                        <li>Quầy bar nhỏ</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="room-item">
                <div class="img">
                    <a href="#"><img src="images/room/room-1.png" alt=""></a>
                </div>
                <div class="info">
                    <h1><a href="#">Phòng Deluxe</a></h1>
                    <ul>
                        <li>Một phòng ngủ, một phòng tắm</li>
                        <li>View quang cảnh vườn hoặc thành phố</li>
                        <li>Giường ngủ rộng</li>
                        <li>Quầy bar nhỏ</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="room-item">
                <div class="img">
                    <a href="#"><img src="images/room/room-1.png" alt=""></a>
                </div>
                <div class="info">
                    <h1><a href="#">Phòng Deluxe</a></h1>
                    <ul>
                        <li>Một phòng ngủ, một phòng tắm</li>
                        <li>View quang cảnh vườn hoặc thành phố</li>
                        <li>Giường ngủ rộng</li>
                        <li>Quầy bar nhỏ</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="room-item">
                <div class="img">
                    <a href="#"><img src="images/room/room-1.png" alt=""></a>
                </div>
                <div class="info">
                    <h1><a href="#">Phòng Deluxe</a></h1>
                    <ul>
                        <li>Một phòng ngủ, một phòng tắm</li>
                        <li>View quang cảnh vườn hoặc thành phố</li>
                        <li>Giường ngủ rộng</li>
                        <li>Quầy bar nhỏ</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="room-item">
                <div class="img">
                    <a href="#"><img src="images/room/room-1.png" alt=""></a>
                </div>
                <div class="info">
                    <h1><a href="#">Phòng Deluxe</a></h1>
                    <ul>
                        <li>Một phòng ngủ, một phòng tắm</li>
                        <li>View quang cảnh vườn hoặc thành phố</li>
                        <li>Giường ngủ rộng</li>
                        <li>Quầy bar nhỏ</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="room-item">
                <div class="img">
                    <a href="#"><img src="images/room/room-1.png" alt=""></a>
                </div>
                <div class="info">
                    <h1><a href="#">Phòng Deluxe</a></h1>
                    <ul>
                        <li>Một phòng ngủ, một phòng tắm</li>
                        <li>View quang cảnh vườn hoặc thành phố</li>
                        <li>Giường ngủ rộng</li>
                        <li>Quầy bar nhỏ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'includes/footer.php' ?>
