<?php include 'includes/header.php' ?>

<?php include 'includes/sub-slide.php' ?>

<section class="block-breadcrumb">
   <div class="container">
      <div class="rcrumbs" id="breadcrumbs">
         <ul>
            <li><a href="#">Home</a><span class="divider">/</span></li>
            <li><a href="#">Hội thảo và tiệc</a></li>
         </ul>
      </div>
   </div>
</section>

<section class="container">
   <div class="workshop-item isRight">
      <div class="row row-collapse com-flex com-flex-middle">
         <div class="col-xs-6 col-xs-push-6">
            <a href="#">
               <img class="img-responsive" src="images/hoithao/hoithao-1.png" alt="">
            </a>
         </div>
         <div class="col-xs-6 col-xs-pull-6">
            <div class="info">
               <h1>Phòng tiệc</h1>
               <p class="line-clamp line-clamp-4">
                  Khách sạn Thắng Lợi Hà Nội nằm ven hồ Tây, hồ rộng và lãng mạn nhất Hà Nội. Từ khách sạn, quý khách chỉ đi mất 30 phút để đên sân bay quốc tế Nội Bài và 10 phút để đi vào trung tâm thành phố. Là khách sạn tiêu chuẩn quốc tế bốn sao, với 175 phòng nghỉ.
               </p>
               <a href="#" class="btn btn-default btn-detail">Chi tiết</a>
            </div>
         </div>
      </div>
   </div>
   <div class="workshop-item isLeft">
      <div class="row row-collapse com-flex com-flex-middle">
         <div class="col-xs-6">
            <a href="#">
               <img class="img-responsive" src="images/hoithao/hoithao-1.png" alt="">
            </a>
         </div>
         <div class="col-xs-6">
            <div class="info">
               <h1>Phòng ăn bufer</h1>
               <p class="line-clamp line-clamp-4">
                  Khách sạn Thắng Lợi Hà Nội nằm ven hồ Tây, hồ rộng và lãng mạn nhất Hà Nội. Từ khách sạn, quý khách chỉ đi mất 30 phút để đên sân bay quốc tế Nội Bài và 10 phút để đi vào trung tâm thành phố. Là khách sạn tiêu chuẩn quốc tế bốn sao, với 175 phòng nghỉ.
               </p>
               <a href="#" class="btn btn-default btn-detail">Chi tiết</a>
            </div>
         </div>
      </div>
   </div>
   <div class="workshop-item isRight">
      <div class="row row-collapse com-flex com-flex-middle">
         <div class="col-xs-6 col-xs-push-6">
            <a href="#">
               <img class="img-responsive" src="images/hoithao/hoithao-1.png" alt="">
            </a>
         </div>
         <div class="col-xs-6 col-xs-pull-6">
            <div class="info">
               <h1>Phòng tiệc</h1>
               <p class="line-clamp line-clamp-4">
                  Khách sạn Thắng Lợi Hà Nội nằm ven hồ Tây, hồ rộng và lãng mạn nhất Hà Nội. Từ khách sạn, quý khách chỉ đi mất 30 phút để đên sân bay quốc tế Nội Bài và 10 phút để đi vào trung tâm thành phố. Là khách sạn tiêu chuẩn quốc tế bốn sao, với 175 phòng nghỉ.
               </p>
               <ul>
                  <li>Phòng họp U 50 người</li>
                  <li>Phòng họp 100 người</li>
                  <li>Phòng họp 150 người</li>
               </ul>
               <a href="#" class="btn btn-default btn-detail">Chi tiết</a>
            </div>
         </div>
      </div>
   </div>

</section>

<?php include 'includes/footer.php' ?>
